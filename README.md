# PayPal coding exercise "Bingo"

## Setup

Install the dependencies

```sh
npm install
```

Start the API server

```sh
node api/index.js
```

Launch the app

```sh
npm start
```

You're all set!

## Under the hood

- [Create React App](https://github.com/facebookincubator/create-react-app)
- [Redux]()
- [Redux-Thunk]()

## If I had more time...

- Unit tests everywhere!
- Better strategy for CSS
- Better UI!
- Redux Saga instead of Thunk

